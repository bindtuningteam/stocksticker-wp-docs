### Language
 
From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).

![Messages](../images/classic/13.advanced.png)

___

### Allow user's symbols

Toggling this option will allow users to add their own stocks to the web part. Note that only the person who added the symbol, will be able to see it, since its definitions are saved within the user.

---
### Max user's symbols

This option allows to define maximum number of user symbols to be added to the web part. The maximum number can, however, be redefined anytime.

![Add section](../images/classic/17.usersymbols.png)