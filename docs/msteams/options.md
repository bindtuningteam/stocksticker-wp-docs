![general options](../images/modern/01.general.png)

<p class="alert alert-success">The information displayed by the Stocks Ticker Web Part is the sole responsibility of the service provider in use. BindTuning cannot guarantee that the displayed information is valid nor the availability of the provided services.</p>

### Provider
    
There are two available providers:

- **IEX** - A free API key for this provider can be requested <a href="https://iexcloud.io/" target="_blank">here</a>. This provider is only available for US tickers.
    
- **Finnhub** - A free API key for this provider can be requested <a href="https://finnhub.io/" target="_blank">here</a>.

After requesting the API key, you'll have to input it in the **API Key** area, for the Web Part to work as intended.    


### Market (Finnhub only)

This option allows you to select between several international markets. By default the options selected is **US Exchanges**.

![general options](../images/modern/14.market.png)

___
### Symbols Search

Search for a ticker symbol or a ticker/mutualfund name. 

- <a href="https://iexcloud.io/console/">Here</a> you can check all the **available symbols** for the **IEX** provider; 
- <a href="https://finnhub.io/docs/api#stock-symbols">Here</a> Here you can check all the **available symbols** for the **Finnhub** provider.

___
### Pinned Symbol

You can pin **only one ticker**. These will show on a top section. on the zone where the Web Part is shown.

Eg. for Microsoft you should write **msft**. When you add the Web Part to the page this will be set by default.  

___
### Symbols

This is where you can add **multiple tickers** symbols to the sliding zone of the Web Part.

![sticker](../images/modern/02.general.opt.png)