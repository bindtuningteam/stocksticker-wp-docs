
###  Rigth to Left

Using this option will change the web part's text orientation to go from right to left. The forms will not be affected.

![sticker](../images/msteams/05.webpartapperence.png)